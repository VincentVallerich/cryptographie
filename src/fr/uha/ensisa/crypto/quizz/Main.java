package fr.uha.ensisa.crypto.quizz;

import fr.uha.ensisa.crypto.quizz.controller.GameController;
import fr.uha.ensisa.crypto.quizz.views.View;
import fr.uha.ensisa.crypto.quizz.views.console.AppConsole;

public class Main {
    public static void main(String[] args) {
        View view = new AppConsole();
        GameController controller = new GameController(view);

        controller.fill();
        controller.startGame();
    }
}
