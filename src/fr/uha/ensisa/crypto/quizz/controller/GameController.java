package fr.uha.ensisa.crypto.quizz.controller;

import fr.uha.ensisa.crypto.quizz.model.dao.QuestionDao;
import fr.uha.ensisa.crypto.quizz.model.question.DaoQuestionFile;
import fr.uha.ensisa.crypto.quizz.model.question.Question;
import fr.uha.ensisa.crypto.quizz.views.View;

public class GameController {
    //    private QuestionDao model = DaoQuestionMem.getInstance();
    private final QuestionDao model = new DaoQuestionFile("quizz.dat");
    private final View view;
    private Question currentQuestion;

    public GameController(View view) {
        this.view = view;
    }

    public void fill() {
        Question question = new Question("Salut", "salut");
        Question question2 = new Question("Salut2", "salut");
        model.add(question);
        model.add(question2);
    }

    public void startGame() {
        String response = null;
        currentQuestion = model.getNext();
        while (currentQuestion != null) {
            view.printQuestion(currentQuestion);
            response = view.getResponseFromUser();
            if (response.equals(currentQuestion.getResponse())) currentQuestion = model.getNext();
        }

        view.printEnd();
    }
}
