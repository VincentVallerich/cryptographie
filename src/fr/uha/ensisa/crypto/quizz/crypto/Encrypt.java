package fr.uha.ensisa.crypto.quizz.crypto;

import fr.uha.ensisa.crypto.quizz.crypto.generator.KeyHolder;
import fr.uha.ensisa.crypto.quizz.model.question.Question;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.GeneralSecurityException;
import java.security.Security;

public class Encrypt {

    public String encrypt(Question question, File file) throws IOException {
        Security.addProvider(new BouncyCastleProvider());
        try {
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
            KeyHolder keyHolder = KeyHolder.getInstance();
            byte[] nonce = keyHolder.getNonce();
            out.write(nonce);

            SecretKey key = keyHolder.getSecretKey();
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(nonce));
            byte[] encryptedString = cipher.doFinal(question.toString().getBytes());
            out.write(encryptedString);

            byte[] cipherFinal = cipher.doFinal();

            out.write(cipherFinal);
            out.close();

            return new String(nonce) + encryptedString + new String(cipherFinal);
        } catch (GeneralSecurityException e){
            e.printStackTrace();
        }
        return null;
    }
}
