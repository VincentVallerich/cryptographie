package fr.uha.ensisa.crypto.quizz.crypto.generator;

import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

public class KeyHolder {

    private KeyHolder() {}

    private static class SingletonHolder{
        private static final KeyHolder instance = new KeyHolder();
    }

    public static KeyHolder getInstance() { return SingletonHolder.instance; }

    public byte[] getNonce() {
        byte[] nonce = new byte[16];
        SecureRandom rand = new SecureRandom();
        rand.nextBytes(nonce);

        return nonce;
    }

    public SecretKey getSecretKey() {
        javax.crypto.KeyGenerator keyGen = null;
        try {
            keyGen = javax.crypto.KeyGenerator.getInstance("AES", "BC");
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        assert keyGen != null;
        return  keyGen.generateKey();
    }
}
