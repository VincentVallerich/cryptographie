package fr.uha.ensisa.crypto.quizz.model.dao;

import fr.uha.ensisa.crypto.quizz.model.question.Question;

public interface QuestionDao {
    public void remove(Question question);
    public void add(Question question);
    public Question getNext();
}
