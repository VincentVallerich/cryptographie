package fr.uha.ensisa.crypto.quizz.model.question;

public class Question {
    private int id = -1;

    private String question;
    private String response;

    public Question(String question, String response){
        this.question = question;
        this.response = response;
    }

    public String getQuestion() { return question; }
    public void setQuestion(String question) { this.question = question; }

    public String getResponse() { return response; }
    public void setResponse(String response) {this.response = response;}

    public int getId() {return id;}
    public void setId(int id) {this.id = id; }

    public static Question serialize(String serializableString){
        String[] splitString = serializableString.split(":");
        return new Question(splitString[0], splitString[1]);
    }

    @Override
    public String toString() {
        return question + ":" + response + "\n";
    }
}
