package fr.uha.ensisa.crypto.quizz.model.question;

import fr.uha.ensisa.crypto.quizz.crypto.Encrypt;
import fr.uha.ensisa.crypto.quizz.model.dao.QuestionDao;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DaoQuestionFile implements QuestionDao {
    private Path filePath;
    private String fileContent;
    private int lineNumber;

    public DaoQuestionFile(String path) {
        this.filePath = Paths.get(path);//new File(path);
//        this.fileContent = getFileContent();
    }

//    public DaoQuestionFile(File file){
//        this.file = file;
//        this.fileContent = getFileContent();
//    }

    public void add(Question question){
        try (FileWriter writer = new FileWriter(filePath.toString(), true)) {
//            if (this.fileContent.contains(question.toString())) return;
            Encrypt encrypt = new Encrypt();
            try {
                if (!isQuestionExist(question))
                    writer.write(question.toString());
//                encrypt.encrypt(question, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    private Question find(int id) {
        String question = null;
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath.toString()))){
            if (existOrCreate()){
                if (lineNumber != 0){
                    int i = 0;
                    question = reader.readLine();
                    if (question != null){
                        while (i != lineNumber){
                            reader.readLine();
                            i++;
                        }
                    }
                }
                question = reader.readLine();
           } else {
               System.out.println("Le fichier n'existe pas");
           }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (question == null) return null;
        lineNumber++;
        return Question.serialize(question); //return decrypt(Question.serialize(question))
    }

    public void remove(Question question) {
        File tmp = null;
        try {
            tmp = File.createTempFile("tmp", ".dat", filePath.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (
                BufferedReader reader = Files.newBufferedReader(filePath)
        ) {
            assert tmp != null;
            try (BufferedWriter writer = Files.newBufferedWriter(tmp.toPath())
            ) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if (!line.contains(question.toString())) {
                        writer.write(line);
                    }
                }
                filePath.toFile().delete();
                tmp.renameTo(filePath.toFile());
                this.filePath = tmp.toPath();
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    private String getFileContent() {
        String content = null;
        try (BufferedReader reader = Files.newBufferedReader(filePath)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return content;
    }

    private boolean existOrCreate() {
        boolean exist = true;
        if (!filePath.toFile().exists()){
            File f = new File(filePath.toString());
            exist = false;
        }
        return exist;
    }

    private boolean isQuestionExist(Question question){
        boolean exist = false;
        try(BufferedReader reader = Files.newBufferedReader(filePath)){
            Encrypt encrypter = new Encrypt();
            String line = reader.readLine();
            while (line != null){
                if (line.equals(question.toString())){
                    exist = true;
                    break;
                }
                line = reader.readLine();
            }
        } catch (IOException e){
            return exist;
        }

        return exist;
    }

    @Override
    public Question getNext() {
        return this.find(lineNumber);
    }
}
