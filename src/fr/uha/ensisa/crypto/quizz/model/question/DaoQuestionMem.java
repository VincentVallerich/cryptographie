package fr.uha.ensisa.crypto.quizz.model.question;

import fr.uha.ensisa.crypto.quizz.model.dao.QuestionDao;

import javax.management.openmbean.KeyAlreadyExistsException;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class DaoQuestionMem implements QuestionDao {
    private HashMap<Integer, Question> pool = new HashMap<>();
    private int questionIndex = 0;

    private DaoQuestionMem() {}

    private static class SingletonHolder{
        private final static DaoQuestionMem instance = new DaoQuestionMem();
    }

    public static DaoQuestionMem getInstance() {
        return SingletonHolder.instance;
    }

    public void add(Question question){
        for (Question q : pool.values()){
            if (q.toString().equals(question.toString())) throw new KeyAlreadyExistsException("Question already exist !");
        }
        int id;
        if (question.getId() == -1){
            id = this.pool.size();
            question.setId(id);
        }
        pool.put(question.getId(), question);
    }

    @Override
    public Question getNext() {
        return null;
    }

    public void remove(Question question){
        boolean find = false;
        for (Question q : pool.values()){
            if (q.getId() == question.getId()){
                remove(question);
                find = true;
            }
        }
        if (!find) throw new NoSuchElementException("Question not exist");
    }

//    public Collection<Question> findAll() {
//        return pool.values();
//    }

    public Question find(int id){
        return pool.getOrDefault(id, null);
    }
}
