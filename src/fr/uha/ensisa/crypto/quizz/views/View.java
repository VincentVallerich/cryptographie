package fr.uha.ensisa.crypto.quizz.views;

import fr.uha.ensisa.crypto.quizz.model.question.Question;

public interface View {
    public void printQuestion(Question question);
    public String getResponseFromUser();
    public void printEnd();
}
