package fr.uha.ensisa.crypto.quizz.views.console;

import fr.uha.ensisa.crypto.quizz.model.question.Question;
import fr.uha.ensisa.crypto.quizz.views.View;

import java.util.Scanner;

public class AppConsole implements View {

    Scanner sc = new Scanner(System.in);

    @Override
    public void printQuestion(Question question) {
        System.out.println(question.getQuestion());
    }

    @Override
    public String getResponseFromUser() {
        System.out.println("Entrez votre réponse : ");
        return sc.nextLine();
    }

    @Override
    public void printEnd() {
        System.out.println("Bravo vous avez gagne !");
    }
}
