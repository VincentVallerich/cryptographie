package fr.uha.ensisa.crypto.quizz.views.gui;

import fr.uha.ensisa.crypto.quizz.model.question.Question;
import fr.uha.ensisa.crypto.quizz.views.View;

public class AppGui implements View {
    @Override
    public void printQuestion(Question question) { }

    @Override
    public String getResponseFromUser() {
        return null;
    }

    @Override
    public void printEnd() { }
}
